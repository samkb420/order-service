const express = require("express");
const app = express();
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const amqp = require("amqplib");

const Order = require("./models/Order");

const isAuthenticated = require("./auth/isAuthenticated");

var channel, connection;

const DBURL =
  "mongodb+srv://sam:12345678sK@cluster0.ubll2.mongodb.net/order-service?retryWrites=true&w=majority";

// mongo
mongoose.connect(
  DBURL,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log("Order-service Connected  to mongodb");
    }
  }
); // connect to mongoDB database called 'order-service'

app.use(express.json());

app.get("/", (req, res) => {
  res.json([
    {
      "Service Name": "Order Service",
      "Service URL": "http://localhost:9090",
    },
    { Status: "Running" },
  ]);
});

async function connect() {
  const amqpServer =
    "amqps://nebdjztn:c6Qv1n5-QyBYu9X0mESdufGyaK8lPUFM@chimpanzee.rmq.cloudamqp.com/nebdjztn";

  connection = await amqp.connect(amqpServer);
  channel = await connection.createChannel();

  await channel.assertQueue("ORDER_QUEUE");
}

function createOrder(products,userEmail){
  let total = 0;

  for(let i = 0; i < products.length; i++){
    total += products[i].price;
  } 

  const order = new Order({
    products,
    user:userEmail,
    total_price:total

  });
  order.save();

  return order;

}
connect().then(() => {
  channel.consume("ORDER_QUEUE", data => {
    const {products ,userEmail} = JSON.parse(data.content);

    const order = createOrder(products, userEmail);

    channel.ack(data);

    channel.sendToQueue("PRODUCT_QUEUE", Buffer.from(JSON.stringify({order})));

    console.log("Consuming Order Queue");
    console.log(products);
  });

}
);

const PORT = process.env.PORT || 9090;
app.listen(PORT, () => {
  console.log(`Order server listening on port ${PORT}`);
});
